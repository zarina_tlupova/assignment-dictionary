%define size_buf 256

extern find_word
extern print_string
extern read_word
extern exit


global _start

section .data
	key: db "key: ",0
	value: db "value: ",0
    if_error: db "Such name does not exist",10,0

%include "./words.inc"

section .text

_start:
	sub rsp, size_buf

	mov rdi, key
	call print_string

	mov rdi, rsp
	mov rsi, size_buf
	call read_word

	mov rdi, word_buf
	mov rsi, first_word
	call find_word
	test rdi, rdi
	jnz .ok

	mov rdi, if_error
	call print_error
	mov rdi, 1
	call exit

	.ok:
		push rdi
		mov rdi, value
		call print_string
		pop rdi
		call print_string
		mov rax, 0
		call exit
